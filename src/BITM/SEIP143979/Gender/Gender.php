<?php
namespace App\BITM\SEIP143979\Gender;
use App\BITM\SEIP143979\Message\Message;
use App\BITM\SEIP143979\Utility\Utility;
use App\BITM\SEIP143979\Model\Database as DB;
use PDO;

class Gender extends DB{
    public $id="";
    public $name="";
    public $gender="";



    public function __construct(){
        parent::__construct();
    }


        public function index($fetchMode='ASSOC'){
            $fetchMode = strtoupper($fetchMode);
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from Gender where is_deleted='0'");
            $sth->execute();
            if(substr_count($fetchMode,'OBJ') > 0)
                $sth->setFetchMode(PDO::FETCH_OBJ);
            else
                $sth->setFetchMode(PDO::FETCH_ASSOC);

            $all_entry=$sth->fetchAll();


            return  $all_entry;
        }





    public function view($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from gender Where id=$id");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $selected_gender=$sth->fetch();

        return  $selected_gender;

    }




    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('gender',$data)){
            $this->gender=$data['gender'];

        }
    }
    public function store(){
       $dbh=$this->connection;
        $values=array($this->name,$this->gender);
        $query="insert into gender(name,gender) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ gender: $this->gender ]
<br> Data Has Been Inserted Successfully!</h3></div>");
        else         Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ gender: $this->gender ]
<br> Data Hasn't Been Inserted Successfully!</h3></div>");



        Utility::redirect('create.php');



    }


    public function update(){
        $dbh=$this->connection;
        $values=array($this->name,$this->gender);

        //var_dump($values);


        $query='UPDATE gender  SET  name = ?   , gender= ? where id ='.$this->id;



        //    $query='UPDATE book_title  SET book_title  = ?   , author_name = ? where id ='.$this->id;

        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ name: $this->name ] ,
               [ Gender: $this->birthday ] <br> Data Has Been Updated Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Updated !</h3></div>");
        Utility::redirect('index.php');



    }
    public function delete($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("delete  from gender Where id=$id");
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has deleted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');


    }
    public function trash($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE gender  SET is_deleted  = "1" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been trashed Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been trashed !</h3></div>");
        Utility::redirect('index.php');



    }


}

