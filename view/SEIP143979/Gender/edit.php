<?php
require_once("../../../vendor/autoload.php");
use App\BITM\SEIP143979\Utility\Utility;
use App\BITM\SEIP143979\Gender\Gender;


$obj= new Gender();
$id=$_GET['id'];



$selected_person= $obj->view($id);
//Utility::dd($selected_person);

?>

    <!DOCTYPE html>
    <html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/gender.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>


<div class="container">


    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Update Gender Form</h1>


                    </div>




                </div>
                <div class="panel-body">


                    <form accept-charset="UTF-8" role="form" class="form-booktitle" method="Post" action="update.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <input class="form-control" value="<?php echo $selected_person['name']?>" name="name" type="text">
                            <label class="h3">Select Gender:</label>
                            <div class="radio">
                                <label><input type="radio" name="gender" value="Male" <?php if($selected_person['gender']=="Male"):?>checked<?php endif ?>>Male</label>
                            </div>
                            <div class="radio ">
                                <label><input type="radio" name="gender" value="Female"<?php if($selected_person['gender']=="Female"):?>checked<?php endif ?>>Female</label>
                            </div>
                            <div class="radio ">
                                <label><input type="radio" name="gender" value="Others"<?php if($selected_person['gender']=="Others"):?>checked<?php endif ?>>Others</label>
                            </div>
                            </br>
                            <input type="hidden" name="id" value="<?php echo $id?>" >
                            <input class="btn btn-lg btn-default btn-block" type="submit" name="submit" value="EDIT">
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>





</body>
</html>