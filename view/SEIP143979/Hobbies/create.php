<?php
require_once("../../../vendor/autoload.php");
use App\BITM\SEIP143979\Message\Message;

if(!isset( $_SESSION)) session_start();
$message=Message::message();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="../../../resource/Bootstrap/css/hobbies.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>


<div class="container">

    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Hobbies</h1>

                    </div>




                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-hobbies" method="Post" action="store.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <input class="form-control" placeholder="Enter Your Name" name="name" type="text">


                                Select your hobbies:<br />
                                <input  type="checkbox" name="hobbies[]" value="GAMING" />GAMING<br />
                                <input  type="checkbox" name="hobbies[]" value="TRAVELLING" />TRAVELLING<br />
                                <input type="checkbox" name="hobbies[]" value="TRACKING" />TRACKING<br />
                                <input  type="checkbox" name="hobbies[]" value="CYCLING" />CYCLING<br />

                            </br>
                            <input class="btn btn-lg btn-default btn-block" type="submit" name="submit" value="SUBMIT">
                        </fieldset>
                    </form>
                    <div id="confirmation_message">
                        <?php echo $message;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    $(document).ready(function(){
        $(function() {
            $('#confirmation_message').delay(5000).fadeOut();
        });

    });
</script>

</body>
</html>